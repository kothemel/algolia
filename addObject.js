const algoliasearch = require('algoliasearch');

const searchClient = algoliasearch(
    'ZE1M6DB26P',
    '06e9a126a39bcb8dccf1482b522f7474'
);
let hits = [];

const index = searchClient.initIndex('my_own_prefix_gear');
index.browseObjects({
    query: '', // Empty query will match all records    
    batch: batch => {
        hits = hits.concat(batch);
    }
})
    .then(function () {
        hits.forEach(hit => {
            index.partialUpdateObject({
                discountedPrice: Number((hit.price - hit.price * hit.discount).toFixed(2)),
                objectID: hit.objectID
            }).then(({ objectID }) => {
                console.log(objectID);
            });
        });
    })
    .catch((e) =>
        console.log(e)
    )