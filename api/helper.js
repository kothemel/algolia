export default () => ({
    replaceWEBP(url) {
        if (url.match(/\.jpg$/))
            return url.replace(/\.jpg$/, ".webp")
        if (url.match(/\.jpeg$/)) return url.replace(/\.jpgeg$/, ".webp")
        if (url.match(/\.png$/)) return url.replace(/\.png$/, ".webp")
        if (url.match(/\.svg$/)) return url.replace(/\.svg$/, ".webp")
    },
    checkAvailability(product) {
        if (product.sell_unit >= 0 && product.availability && product.sell_quantity - product.sell_unit > 0)
            return product.sell_quantity - product.sell_unit;
        return 0;
    },
})