import helper from "../api/helper"

export default (ctx, inject) => {
    const factories = {
        helper: helper()
    }
    inject('api', factories)
}
